
public class Quiz3and4 {
	public static void main(String[] args) {
		// 1. Length of the string 	
		String str = "copyandpastecopyandpastecopyandpastecopyandpastecopyandpastecopyandpastecommitandpushcommitandpushcommitandpushcommitandpushpushcccccommmitttsss";
		System.out.println("The total number of characters in the sentence :" + str.length());
		
		// 2. The last 15 characters in the string
		System.out.println("The last 15 characters in the string : "+ str.substring(str.length() - 15));
		
		// 3. The number of 'c' and 'p'
		
		int numberC = 0;
		int numberP = 0;
		
		for (char ch : str.toCharArray()) {
			if(ch == 'c') {
				numberC++;
			}else if(ch == 'p') {
				numberP++;
			}
		}
		System.out.println("Number of times for c:"+ numberC);
		System.out.println("Number of times for p:"+ numberP);
	}
}
